#coded by diegocbolsoni

import re
from csv import DictWriter
from dataclasses import dataclass, field
from datetime import datetime, timedelta
from functools import reduce
from os import listdir
from typing import List, Dict, Callable


class Log:
    level_pattern = '(?P<level>[A-Z]{4,8}).*'
    date_pattern = '(?P<date>\d{4}-\d{2}-\d{2})'

    def __init__(self, line: str, num_line: int = None):
        self.line = line
        self.num_line = num_line
        self._level = None
        self._date = None

    @staticmethod
    def _search(pattern, text, default_value):
        result = re.search(pattern, text)
        if not result:
            return default_value
        return result.groups()[0]

    @property
    def level(self):
        if not self._level:
            self._level = self._search(self.level_pattern, self.line, "LEVEL")
        return self._level

    @property
    def date(self):
        if not self._date:
            date_text = self._search(self.date_pattern, self.line, "DATE")
            if date_text != "DATE":
                self._date = datetime.strptime(date_text, "%Y-%m-%d")
        return self._date


@dataclass
class LogParser:
    """
    Concatenador e Leitor com filtros para Logs
    :param folder -> pasta com os logs com extensao [.log]
    :param filenames OPTIONAL -> arquivos contendo os logs
    ['arquivo1.log', 'arquivo2.log']
    :param files_logs OPTIONAL -> lista de arquivos com seus logs
    {'arquivo1.log': [log01, log02, log03], 'arquivo2.log': [log01, log02]

    Para executar bastar informar a pasta com os logs. exemplo:
        LogParser('logs/')
    Para exportar os logs filtrados basta chamar a funcao save
    com o caminho do arquivo. exemplo:
        log_parser = LogParser('logs/')
        log_parser.filtrar_por_level("DEBUG")
        log_parser.save('output/log_filtrados.txt')
    """
    folder: str = None
    filenames: List[str] = field(default_factory=list)
    files_logs: dict = field(default_factory=dict)

    @property
    def total_logs(self):
        return reduce(lambda total, file: total + len(self.files_logs[file]),
                      self.files_logs, 0)

    def _load_filenames(self):
        self.filenames = [filename
                          for filename in listdir(self.folder)
                          if filename[-3:] == "log"]

    def filtrar_por_data(self, data_inicio: str, data_final: str):
        data_inicio = datetime.strptime(data_inicio, "%Y-%m-%d")
        data_final = datetime.strptime(data_final, "%Y-%m-%d")
        filtro = lambda log: log.date and data_inicio <= log.date <= data_final
        self.filtros(self.files_logs, filtro)

    def filtrar_por_level(self, level: str):
        filtro = lambda log: log.level == level.upper()
        self.filtros(self.files_logs, filtro)

    def filtrar_por_texto(self, texto: str):
        filtro = lambda log: re.search(texto, log.line, re.IGNORECASE)
        self.filtros(self.files_logs, filtro)

    @staticmethod
    def filtros(
            files_logs: dict,
            filtro: Callable
    ) -> None:
        for filename, logs in files_logs.items():
            files_logs[filename] = list(filter(filtro, logs))

    @staticmethod
    def _read_logs_from_file(filepath: str) -> List[Log]:
        with open(filepath, 'r') as f:
            return [
                Log(line.strip(), i)
                for i, line in enumerate(f.readlines())
                if line.strip()]

    def _load_logs(self):
        for file in self.filenames:
            self.files_logs[file] = self._read_logs_from_file(
                self.folder + file)

    def load(self):
        if not self.filenames:
            self._load_filenames()
        self._load_logs()

    @staticmethod
    def sort_by_date(logs: List[Log], reverse=True) -> List[Log]:
        return sorted(logs, key=lambda log: log.date, reverse=reverse)

    @staticmethod
    def generate_row(fieldnames: List[str], file: str, log: Log) -> dict:
        return {
            fieldnames[0]: file,
            fieldnames[1]:
                log.date.strftime("%Y-%m-%d") if log.date else "SEM DATA",
            fieldnames[2]: log.level,
            fieldnames[3]: log.num_line,
            fieldnames[4]: log.line
        }

    @staticmethod
    def write(writer, files_logs: Dict):
        writer.writeheader()
        for file, logs in files_logs.items():
            for log in logs:
                writer.writerow(
                    LogParser.generate_row(writer.fieldnames, file, log))

    def save(self, output_path: str):
        if not self.files_logs:
            raise Exception("Sem logs para salvar.")
        with open(output_path, 'w') as f:
            fieldnames = ['arquivo', 'data', 'level', 'linha', 'log']
            writer = DictWriter(f, fieldnames=fieldnames)
            self.write(writer, self.files_logs)
