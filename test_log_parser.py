#coded by diegocbolsoni

from unittest import TestCase
from unittest.mock import MagicMock, patch, mock_open
from log_parser import LogParser, Log
from pytest import mark


@mark.parametrize("line,date,level",
                  [
                      ["2022-08-11 00:01:33.994  "
                       "ISSAO 3156 --- [Eureka-EvictionTimer]"
                       " c.n.e.registry.AbstractInstanceRegistry  : "
                       "Running the evict task with compensationTime 0ms",
                       "2022-08-11",
                       "ISSAO"],
                      ["2021-12-20 22:04:49 [container-15384] INFO  "
                       "c.i.a.m.service.impl.ConsumeService - "
                       "Gravando registro MediaServerOutput",
                       "2021-12-20",
                       "INFO"],
                      ["2021-12-22 15:20:27 [container-15384] ERROR "
                       "c.i.a.m.service.impl.ConsumeService - "
                       "A porta não está rodando nenhum stream, abortando...",
                       "2021-12-22",
                       "ERROR"],
                      ["2021-12-20 22:06:53 [container-15384] DEBUG "
                       "c.i.a.m.service.impl.ConsumeService - "
                       "Número de locutores salvos: 0",
                       "2021-12-20",
                       "DEBUG"]
                  ])
def test_log_patterns(line, date, level):
    log = Log(line, 2)
    assert date == log.date.strftime("%Y-%m-%d")
    assert level == log.level


class TestALogParser(TestCase):
    def test_instance(self):
        folder_path = MagicMock()
        lp = LogParser(folder_path)
        self.assertIsInstance(lp, LogParser)

    @patch("log_parser.listdir", return_value=['1.log', '2.log', '3.log'])
    def test_load_filenames(self, mock_listdir: MagicMock):
        folder_path = MagicMock()
        lp = LogParser(folder_path)
        lp._load_filenames()
        mock_listdir.assert_called_once_with(folder_path)
        self.assertEqual(['1.log', '2.log', '3.log'], lp.filenames)

    @patch('log_parser.Log')
    def test_read_logs_from_file(self, mock_log: MagicMock):
        with patch("builtins.open", mock_open(read_data="data")) as mock_file:
            result = LogParser._read_logs_from_file(MagicMock())
            self.assertEqual([mock_log()], result)

    @patch.object(LogParser, '_load_logs')
    @patch.object(LogParser, '_load_filenames')
    def test_load(self,
                  mock_load_filenames: MagicMock,
                  mock_load_logs: MagicMock):
        lp = LogParser(MagicMock())
        lp.load()
        mock_load_filenames.assert_called_once()
        mock_load_logs.assert_called_once()

    @patch.object(LogParser, 'write')
    @patch("log_parser.DictWriter")
    def test_save(self,
                  mock_dictwriter: MagicMock,
                  mock_write: MagicMock):
        lp = LogParser(MagicMock())
        lp.files_logs = {"1": [1, 2]}
        with patch("builtins.open", mock_open(read_data="data")) as mock_file:
            lp.save('test.txt')
            mock_dictwriter.assert_called_once_with(
                mock_file(),
                fieldnames=['arquivo', 'data', 'level', 'linha', 'log']
            )
            mock_write.assert_called_once_with(mock_dictwriter(),
                                               {"1": [1, 2]})

    def test_write(self, ):
        lp = LogParser(MagicMock())
        log = Log(
            "2021-12-20 22:04:49 INFO c.i.a.m.service.impl.ConsumeService"
        )
        files_logs = {
            "filename": [
                log
            ]
        }
        writer = MagicMock()
        lp.write(writer, files_logs)
        writer.writeheader.assert_called_once()
        writer.writerow.assert_called_once_with({
            writer.fieldnames[0]: "filename",
            writer.fieldnames[1]: log.date,
            writer.fieldnames[2]: log.level,
            writer.fieldnames[3]: log.num_line,
            writer.fieldnames[4]: log.line
        })

    def test_filtrar_por_texto(self):
        log = Log("log 01", 1)
        lp = LogParser(MagicMock())
        lp.files_logs = {'file1': [
            log,
            Log("log 02", 2),
            Log("log 03", 3),
        ]}
        lp.filtrar_por_texto("01")
        self.assertEqual([log], lp.files_logs['file1'])

    def test_filtrar_por_level(self):
        log = Log("log ERROR 01", 1)
        lp = LogParser(MagicMock())
        lp.files_logs = {'file1': [
            log,
            Log("DEBUG log 02", 2),
            Log("INFO log 03", 3),
        ]}
        lp.filtrar_por_level("ERROR")
        self.assertEqual([log], lp.files_logs['file1'])

    def test_filtrar_por_data(self):
        log = Log("2021-02-02 log ERROR 01", 1)
        lp = LogParser(MagicMock())
        lp.files_logs = {'file1': [
            log,
            Log("2022-01-01 DEBUG log 02", 2),
            Log("2022-02-02 INFO log 03", 3),
        ]}
        lp.filtrar_por_data("2021-01-01", "2021-02-02")
        self.assertEqual([log], lp.files_logs['file1'])
