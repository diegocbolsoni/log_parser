import sys
from dataclasses import dataclass

from log_parser import LogParser


@dataclass
class App:
    folder_path: str = None
    output_file_path: str = None
    log_parser: LogParser = None
    _menu_option: str = None

    def display_stats(self):
        print("-" * 50)
        print(f"Folder: {self.folder_path} -> Output: {self.output_file_path}")
        print(f'[{len(self.log_parser.filenames)}] arquivos abertos.')
        print(f"[{self.log_parser.total_logs}] logs em memoria.")

    def ask_folder_path(self):
        print()
        self.folder_path = input(
            "Qual o caminho do folder que contem os logs? ")
        self.folder_path = self.folder_path \
            if self.folder_path[:-1] == "/" else f"{self.folder_path}/"
        print()

    def ask_output_file_path(self):
        print()
        self.output_file_path = input(
            "Qual o caminho e o nome do arquivo que sera exportado? ")
        print()

    def load(self):
        print()
        print('#' * 50)
        print("# concatenador e filtros de logs #".upper())
        print("#" * 50)
        print('loading...')
        if not self.folder_path:
            self.ask_folder_path()
        if not self.output_file_path:
            self.ask_output_file_path()
        self.log_parser.folder = self.folder_path
        self.log_parser.load()

    def display(self):
        self.display_stats()
        print("-" * 50)
        print("1 - concatenar e filtrar por level")
        print("2 - concatenar e filtrar por data")
        print("3 - concatenar e filtrar por palavra")
        print("4 - somente concatenar")
        print("5 - sair "
              "(ou aperte ctrl-|, ctrl-z ou ctrl-c a qualquer momento)")
        print("6 - restart (re-abrir os arquivos de log)")
        print("-" * 50)
        self._menu_option = input("Digite a opcao e pressione ENTER: ")

    def run(self):
        while not self._menu_option:
            self.display()
            self.handle_input()
            self.display_stats()
            exportar = input('Digite 1 para exportar e 2 para voltar: ')
            if exportar == "1":
                self.export()
            else:
                self._menu_option = ""

    def handle_input(self):
        if self._menu_option == "1":
            level = input("Digite o level: ")
            self.log_parser.filtrar_por_level(level)
        if self._menu_option == "2":
            data_initial = input(
                "Digite a data inicial inclusive(formato ANO-MES-DIA): ")
            data_final = input(
                "Digite a data final inclusive(formato ANO-MES-DIA): ")
            self.log_parser.filtrar_por_data(data_initial, data_final)
        if self._menu_option == "3":
            texto = input("Digite a palavra ou texto: ")
            self.log_parser.filtrar_por_texto(texto)
        if self._menu_option == "5":
            sys.exit(1)

    def export(self):
        print("-" * 50)
        self.log_parser.save(self.output_file_path)
        print(f"Um total de [{self.log_parser.total_logs}] logs.")
        print(f"Arquivo [{self.output_file_path}] gerado.")


if __name__ == "__main__":
    fp, op = None, None
    if len(sys.argv) > 1:
        fp = sys.argv[1]
    if len(sys.argv) > 2:
        op = sys.argv[2]
    app = App(fp, op, LogParser())
    app.load()
    app.run()
